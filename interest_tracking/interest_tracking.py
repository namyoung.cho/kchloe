import datetime
import json
import requests
import asyncio
import numpy as np
import pandas as pd
from statsmodels.tsa.holtwinters import ExponentialSmoothing
from pytrends.request import TrendReq
from statsmodels.tsa.exponential_smoothing.ets import ETSModel

#history_data.json 파일 읽는 함수 arr[dict]반환
def read_json():
    try:
        response = requests.get('http://34.64.100.140/file/download/history_data.json')
        data = response.json()
        json_data = json.loads(data)
        return json_data["data"]

    except json.JSONDecodeError:
        print("error: not json file")
        return None
    
    except:
        print("error: can't find json")
        return None


#interest_tracking.json파일을 서버에 업로드하는 함수
def send_json(data):
  json_data = json.dumps(data, ensure_ascii=False)
  file_name = "interest_tracking.json"
  files = {"file": (file_name, json_data)}
  url = "http://34.64.100.140/file/upload"
  response = requests.post(url, files=files)

  if response.status_code == 200:
    print('File uploaded successfully')
  else:
    print('Error uploading file:', response.status_code)


#get data from google trend
def getTsData(keyword):
  today = datetime.date.today()
  five_years_ago = today - datetime.timedelta(days=365*5)

  pytrends = TrendReq(hl = 'ko-KR', tz=540)
  keyword_list = [keyword]
  pytrends.build_payload(keyword_list, timeframe='today 5-y')
  df = pytrends.interest_over_time()

  index = pd.date_range(five_years_ago, periods=len(df), freq='W')

  df_ts = pd.Series(df[keyword], index=index)

  return df_ts

#세 가지 ets모델 중 AICc값이 가장 작은 ets모델 선정하여 반환 
def getEtsModel(df):
  model1 = ExponentialSmoothing(df, trend="multiplicative",seasonal="multiplicative")
  model2 = ExponentialSmoothing(df, trend="add",seasonal="add")
  model3 = ExponentialSmoothing(df, trend="add",seasonal="multiplicative")

  models = [model1, model2, model3]
  AICc_mean = 10000

  for model in models:
    fit = model.fit()
    AICc = fit.aicc
    if AICc_mean >= AICc:
      AICc_mean = AICc
      fit_ets = fit

  return fit_ets

#ets모델 적용하여, 다음 주 예측값 - 이번 주 값 >=7인 키워드만 반환
def task(keywords):
  send_keyword = []

  for keyword in keywords:
    df = getTsData(keyword)
    fit = getEtsModel(df)
    fc = fit.predict(start=len(df)+1, end=len(df)+1) #next week predict data
    this_week = df.tail(1)[0] #this week data

    if fc[0]-this_week >= 7:
      send_keyword.append(keyword)

  return send_keyword

def main():
  json_datas = read_json()
  send_data = []

  for json_data in json_datas:
    customer = json_data["customer"]
    keywords = json_data["keywords"]

    send_keyword = task(keywords)

    temp_dict = {'customer': customer, 'keywords': send_keyword}
    send_data.append(temp_dict)

  send_dict = {'data': send_data}

  send_json(send_dict)

main()
  



