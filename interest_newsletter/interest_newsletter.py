import json
import requests
from jinja2 import Template
import asyncio
import threading
from pipedream.script_helpers import (steps, export)

#MOYA_url with token
MOYA_url = "https://api.moya.ai/news?token=w83yEMuIeJkgWtU1opETFAhYPunDFs"


#MOYA api 사용하는 함수 input = url, output = arr[list]
def search_MOYA(url):        
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        print("Http "+str(response.status_code) + url)
    else:
        print("error:" + url + "Moya api down")
    return data['datas']


#src/json/history_data.json 파일 읽는 함수 arr[list]반환
def read_json():
    try:
        response = requests.get('http://34.64.100.140/file/download/history_data.json')
        data = response.json()
        json_data = json.loads(data)
        return json_data["data"]

    except json.JSONDecodeError:
        print("error: not json file")
        return None
    
    except:
        print("error: can't find json")
        return None

#url 만드는 함수 한 customer당 최대 5개. input = arr[list], output = urls[]
def make_url(data):
        keyword = data['keywords']
        urls = []
        #scrape Moya result
        for key in keyword: 
            keyword_url = MOYA_url + "&keyword=" + key
            urls.append(keyword_url)
        return urls

#html 파일 만드는 함수 text = html형식 rendering_tmpl에서 받음
def send_html(customer, template):
    file_name = customer + ".html"
    files = {"file": (file_name, template)}
    url = "http://34.64.100.140/file/upload"
    response = requests.post(url, files=files)
    if response.status_code == 200:
        print('File uploaded successfully')
    else:
        print('Error uploading file:', response.status_code)

#MOYA 결과 바탕으로 html template 만드는 함수
def rendering_tmpl(data):
    tmpl = Template(u'''\
    <!DOCTYPE html>
<html>
<head>
    <meta charset = 'utf-8'>
    <title>test</title>
</head>

<body>
    <h2>{{title0}}</h2><br>
    <a href="{{link0}}">{{summarized0}}</a> 
    <h2>{{title1}}</h2><br>
    <a href="{{link1}}">{{summarized1}}</a> 
    <h2>{{title2}}</h2><br>
    <a href="{{link2}}">{{summarized2}}</a> 
    <h2>{{title3}}</h2><br>
    <a href="{{link3}}">{{summarized3}}</a> 
    <h2>{{title4}}</h2><br>
    <a href="{{link4}}">{{summarized4}}</a>     
</body>

</html>
''')
    rend = tmpl.render(
        title0 = data[0]['title'],
        link0 = data[0]['url'],
        summarized0 = data[0]['summarized'],
        title1 = data[1]['title'],
        link1 = data[1]['url'],
        summarized1 = data[1]['summarized'],
        title2 = data[2]['title'],
        link2 = data[2]['url'],
        summarized2 = data[2]['summarized'],
        title3 = data[3]['title'],
        link3 = data[3]['url'],
        summarized3 = data[3]['summarized'],
        title4 = data[4]['title'],
        link4 = data[4]['url'],
        summarized4 = data[4]['summarized']
        )
    return rend

def task(customer, urls):
    search_results = []
    for url in urls: #생성된 url 수만큼 반복 (최대 5)
        search_result = search_MOYA(url) #뉴스 탐색 list형식으로 받음
        search_results.append(search_result[0]) #탐색 결과 search_results에 저장
        
    html_result = rendering_tmpl(search_results) #html template 생성
    send_html(customer, html_result) #html 파일로 반환


#----main----
threads = []

def main():
    datas = read_json() 
    
    for data in datas:  #datas arr 수만큼 반복
        search_results = [] #한 사람당 keyword 결과를 담기 위한 arr
        customer = data['customer'] #customer data
        urls = make_url(data) #url 생성

        work = threading.Thread(target=task, args=(customer, urls,))
        work.daemon = True
        work.start()
        threads.append(work)

    for thread in threads:
        thread.join()




main()